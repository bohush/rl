<?php

require "vendor/predis/autoload.php";


Predis\Autoloader::register();


class DB
{
    /**
     * @var PredisClient
     */
    protected $db;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        try {
            $this->db = new Predis\Client([
                "scheme" => "tcp",
                "host" => "127.0.0.1",
                "port" => 6379
            ]);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function exists($key): bool
    {
        return (bool)$this->db->exists($key);
    }

    public function ttl($key): int
    {
        return $this->db->ttl($key);
    }

    /**
     * @param $key
     * @param $value
     * @param $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl)
    {
        return $this->db->set($key, (string)$value, "EX", $ttl);
    }

    /**
     * @return float
     */
    public function get($key): float
    {
        return (float)$this->db->get($key);
    }

    public function keys($keys)
    {
        return $this->db->keys($keys);
    }
}
