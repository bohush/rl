<?php
require 'DB.php';


class RL
{

    protected $max_req;
    protected $period;
    protected $time_to_ban;
    protected $ip;
    private $redis;

    /**
     * RL constructor.
     * @param int $max_req
     * @param int $period in seconds
     */
    public function __construct($max_req, $period, $time_to_ban)
    {
        $this->max_req = $max_req;
        $this->period = $period;
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->time_to_ban = $time_to_ban;
        $this->redis = new DB();
    }

    public function access()
    {
        $b_key = $this->getBanKey();
        $r_key = $this->getReqKey() . microtime(true);

        if ($this->redis->exists($b_key)) {
            $r_a = $this->redis->ttl($b_key);
            $this->setBanHeader($r_a);
            return false;
        }

        $cnt_prev_req = count($this->redis->keys($this->getReqKey() . "*"));
        if ($cnt_prev_req == $this->max_req) {
            $this->redis->set($b_key, time(), $this->time_to_ban);
            $r_a = $this->redis->ttl($b_key);
            $this->setBanHeader($r_a);
            return false;
        } else {
            $this->redis->set($r_key, time(), $this->period);
            return true;
        }
    }

    /*
     * @return string
     */
    private function getReqKey()
    {
        return "req:{$this->ip}:";
    }

    /*
     * @return string
     */
    private function getBanKey()
    {
        return "ban:{$this->ip}";
    }

    /**
     * @param $r_a
     */
    private function setBanHeader($r_a)
    {
        http_response_code(429);
        header('Retry-After: ' . $r_a);
    }

}
